package GUI;

import Main.Mapper;
import Utils.Disease;
import Utils.Drug;
import Utils.MedicalItem;
import Utils.Results;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Scanner;
import javax.swing.JTextField;
import java.awt.GridLayout;

public class Frame {

	private JFrame frame;
	private JTextField textField;
	private boolean drug=false;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Frame window = new Frame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Frame() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new BorderLayout(0, 0));

		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new BorderLayout(0, 0));

		JPanel panel_1 = new JPanel();
		panel.add(panel_1, BorderLayout.NORTH);

		JLabel Welcome = new JLabel("Welcome to uHealthCare*");
		panel_1.add(Welcome);

		JPanel panel_2 = new JPanel();
		panel.add(panel_2);

		textField = new JTextField();
		panel_2.add(textField);
		textField.setColumns(10);
		

		String[] a = { "Disease", "Drug" };
		final JComboBox comboBox_4 = new JComboBox();
		for (String c : a)
			comboBox_4.addItem(c);
		panel_2.add(comboBox_4);

		JButton btnConnect = new JButton("Fire !");
		panel_2.add(btnConnect);

		JPanel panel2 = new JPanel();
		frame.getContentPane().add(panel2, BorderLayout.CENTER);
		panel2.setLayout(new BorderLayout(0, 0));

		JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		panel2.add(tabbedPane);

		JPanel panel_4 = new JPanel();
		tabbedPane.addTab("HeadledBy", null, panel_4, null);
		

		final JComboBox comboBox_1 = new JComboBox();
		panel_4.add(comboBox_1);
		
		final JLabel Information1 = new JLabel("Information");
		panel_4.add(Information1);

		JPanel panel_5 = new JPanel();
		tabbedPane.addTab("CausedBy", null, panel_5, null);

		final JComboBox comboBox_2 = new JComboBox();
		panel_5.add(comboBox_2);
		
		final JLabel Information2 = new JLabel("Information");
		panel_5.add(Information2);

		JPanel panel_6 = new JPanel();
		tabbedPane.addTab("Symptoms", null, panel_6, null);

		final JComboBox comboBox = new JComboBox();
		panel_6.add(comboBox);
		
		final JLabel Information = new JLabel("Information");
		panel_6.add(Information);

		JPanel panel_3 = new JPanel();
		tabbedPane.addTab("Parents", null, panel_3, null);

		final JComboBox comboBox_3 = new JComboBox();
		panel_3.add(comboBox_3);
		
		final JLabel Information4 = new JLabel("Information");
		panel_3.add(Information4);
			
		Mapper.prepare();
		
		comboBox_4.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
					if(e.getItem().toString().equals("Drug")){
						drug=true;
					}else{
						drug=false;
					};
			}
		});
		
	btnConnect.addActionListener(new ActionListener() {
		
		
		
		public void actionPerformed(ActionEvent e){
			
			
			
			System.out.println("Please enter a disease (q for quit) :");
			
			//while(!str.equals("q")){
				final Results r;
				
				try {
					
				
					if(drug)
					r = Mapper.handleRequest("%"+textField.getText());
					else
					r = Mapper.handleRequest(textField.getText());	
					r.print();
					
					for(MedicalItem a: r.getHealedBy().getResults()){
						comboBox_1.addItem(a);
					}
					for(MedicalItem a: r.getCausedBy().getResults()){
						comboBox_2.addItem(a);
					}
					for(MedicalItem a: r.getParents().getResults()){
						comboBox_3.addItem(a);
					}
					for(MedicalItem a: r.getSymptoms().getResults()){
						comboBox.addItem(a);
					}
					
					comboBox_1.addItemListener(new ItemListener() {
						
						@Override
						public void itemStateChanged(ItemEvent e) {
							
								Drug d=new Drug(e.getItem().toString(),null,null);
								for(MedicalItem a:r.getHealedBy().getResults()){
									int l=r.getHealedBy().getResults().indexOf(d);
									d = (Drug) r.getHealedBy().getResults().get(l);
									Information1.setText(d.getInfo().toString());
								}
						}
					});
					
					comboBox_2.addItemListener(new ItemListener() {
						
						@Override
						public void itemStateChanged(ItemEvent e) {
							
								Drug d=new Drug(e.getItem().toString(),null,null);
								for(MedicalItem a:r.getCausedBy().getResults()){
									int l=r.getCausedBy().getResults().indexOf(d);
									d = (Drug) r.getCausedBy().getResults().get(l);
									Information2.setText(d.getInfo().toString());
								}
						}
					});
					comboBox_3.addItemListener(new ItemListener() {
						
						@Override
						public void itemStateChanged(ItemEvent e) {
							
								Disease d=new Disease(e.getItem().toString(),null,null);
								for(MedicalItem a:r.getParents().getResults()){
									int l=r.getParents().getResults().indexOf(d);
									d = (Disease) r.getParents().getResults().get(l);
									Information4.setText(d.getInfo().toString());
								}
						}
					});
					comboBox.addItemListener(new ItemListener() {
						
						@Override
						public void itemStateChanged(ItemEvent e) {
							
								Disease d=new Disease(e.getItem().toString(),null,null);
								for(MedicalItem a:r.getSymptoms().getResults()){
									int l=r.getSymptoms().getResults().indexOf(d);
									d = (Disease) r.getSymptoms().getResults().get(l);
									Information.setText(d.getInfo().toString());
								}
						}
					});
					
					//System.out.println("Please enter a disease (q for quit) :");
					//str = sc.nextLine();
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
			//}
			
			//sc.close();
			Mapper.close();
			
		}
		
		
	});
	
}
	
}
