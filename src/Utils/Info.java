package Utils;

import java.util.ArrayList;
import java.util.Collection;

public class Info {
	
	public enum Source{
		OMIM,
		Sider,
		DrugBank,
		OrphaData,
		OMIMHypothesis,
		SiderHypothesis,
		DrugBankHypothesis,
		OrphaDataHypothesis,
		Conflict
	}
	
	private String content;
	private int[] sources;
	
	public Info(String content, Source source){
		this.content = content;
		this.sources = new int[9];
		sources[source.ordinal()]++;
	}

	public String getContent() {
		return content;
	}
	
	public void makeHypothesis(){
		sources[0] += sources[4];
		sources[1] += sources[5];
		sources[2] += sources[6];
		sources[3] += sources[7];
		sources[4] = 0;
		sources[5] = 0;
		sources[6] = 0;
		sources[7] = 0;

	}

	public int[] getSources() {
		return sources;
	}
	
	public void addSource(int[] src){
		for(int i = 0; i < sources.length; i ++){
			sources[i] += src[i];
		}
	}
	
	public void addSource(Source s){
		sources[s.ordinal()] ++;
	}
	
	public boolean equals(Object o){
		Info i = (Info) o;
		return content.equals(i.content);
	}
	
	public String toString(){
		String result = content + " sources :";
		Info.Source[] src = Info.Source.values();
		for(int i = 0; i < sources.length; i ++){
			if(sources[i] > 0)
				result += " " + src[i].name() + " : " + sources[i];
		}
		return result;
	}
	
}
