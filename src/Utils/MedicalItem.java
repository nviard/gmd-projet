package Utils;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public abstract class MedicalItem {

	private HashSet<String> names;
	private ArrayList<Info> info;
	private ArrayList<Info> mappingExcludedInfo;

	private MedicalItem() {
		names = new HashSet<String>();
		info = new ArrayList<Info>();
		mappingExcludedInfo = new ArrayList<Info>();
	}

	public MedicalItem(String name, Info mappingExcludedInfo, Info... infos) {
		this();
		this.parseOwnDictionary(name, this.names);
		if(infos != null)
			for (Info i : infos)
				this.addInfo(i);
		if (mappingExcludedInfo != null)
			this.addMappingExcludedInfo(mappingExcludedInfo);
	}
	
	protected abstract void parseOwnDictionary(String s, Collection<String> c) ;

	public boolean containsNames() {
		return !names.isEmpty();
	}

	public boolean equals(Object o) {
		MedicalItem d = (MedicalItem) o;
		return names.size() == d.names.size() && names.containsAll(d.names);
	}

	public String toString() {
		String result = "";
		for (String s : names) {
			result += ";" + s;
		}
		if (result.length() > 0) {
			result = result.substring(1, result.length());
		}
		return result;
	}

	public void addName(String n) {
		names.add(n);
	}

	public void addInfo(Collection<Info> c, boolean hypothesis) {
		for (Info i : c) {
			addInfo(i, hypothesis);
		}
	}

	public void addInfo(Collection<Info> c) {
		addInfo(c, false);
	}

	public void addInfo(Info i, boolean hypothesis) {
		if (hypothesis) {
			i.makeHypothesis();
		}
		int index = info.indexOf(i);
		if (index == -1)
			info.add(i);
		else
			info.get(index).addSource(i.getSources());
	}

	public void addInfo(Info i) {
		addInfo(i, false);
	}

	public void addMappingExcludedInfo(Collection<Info> c, boolean hypothesis) {
		for (Info i : c) {
			addMappingExcludedInfo(i, hypothesis);
		}
	}

	public void addMappingExcludedInfo(Collection<Info> c) {
		addMappingExcludedInfo(c, false);
	}

	public void addMappingExcludedInfo(Info i, boolean hypothesis) {
		if (hypothesis) {
			i.makeHypothesis();
		}
		int index = mappingExcludedInfo.indexOf(i);
		if (index == -1)
			mappingExcludedInfo.add(i);
		else
			mappingExcludedInfo.get(index).addSource(i.getSources());
	}

	public void addMappingExcludedInfo(Info i) {
		addMappingExcludedInfo(i, false);
	}

	public AbstractList<Info> getInfo() {
		return info;
	}

	public AbstractList<Info> getMappingExcludedInfo() {
		return mappingExcludedInfo;
	}

}
