package Utils;

import java.util.HashSet;

public class Results {



	private HashSet<String> synonyms;

	private MedicalCollection parents;
	private MedicalCollection symptoms;
	private MedicalCollection causedBy;
	private MedicalCollection healedBy;
	
	public Results() {
		super();
		this.synonyms = new HashSet<String>();
		this.parents = new MedicalCollection();
		this.symptoms = new MedicalCollection();
		this.causedBy = new MedicalCollection();
		this.healedBy = new MedicalCollection();
	}
	
	/**
	 * @return the synonyms
	 */
	public HashSet<String> getSynonyms() {
		return synonyms;
	}
	/**
	 * @param synonyms the synonyms to set
	 */
	public void setSynonyms(HashSet<String> synonyms) {
		this.synonyms = synonyms;
	}
	/**
	 * @return the parents
	 */
	public MedicalCollection getParents() {
		return parents;
	}
	/**
	 * @param parents the parents to set
	 */
	public void setParents(MedicalCollection parents) {
		this.parents = parents;
	}
	/**
	 * @return the symptoms
	 */
	public MedicalCollection getSymptoms() {
		return symptoms;
	}
	/**
	 * @param symptoms the symptoms to set
	 */
	public void setSymptoms(MedicalCollection symptoms) {
		this.symptoms = symptoms;
	}
	/**
	 * @return the causedBy
	 */
	public MedicalCollection getCausedBy() {
		return causedBy;
	}
	/**
	 * @param causedBy the causedBy to set
	 */
	public void setCausedBy(MedicalCollection causedBy) {
		this.causedBy = causedBy;
	}
	/**
	 * @return the healedBy
	 */
	public MedicalCollection getHealedBy() {
		return healedBy;
	}
	/**
	 * @param healedBy the healedBy to set
	 */
	public void setHealedBy(MedicalCollection healedBy) {
		this.healedBy = healedBy;
	}
	
	public Results inter(Results r){
		this.synonyms.addAll(r.synonyms);
		this.parents.inter(r.parents);
		this.symptoms.inter(r.symptoms);
		this.healedBy.inter(r.healedBy);
		this.causedBy.inter(r.causedBy);
		return this;
	}
	
	public Results union(Results r){
		this.synonyms.addAll(r.synonyms);
		this.parents.union(r.parents);
		this.symptoms.union(r.symptoms);
		this.healedBy.union(r.healedBy);
		this.causedBy.union(r.causedBy);
		return this;
	}
	
	public void print(){
		System.out.println("====Synonyms");
		for(String s : synonyms){
			System.out.println(s);
		}
		System.out.println("====Parents");
		this.parents.print();
		System.out.println("====Symptoms");
		this.symptoms.print();
		System.out.println("====Healed by");
		this.healedBy.print();
		System.out.println("====Caused by");
		this.causedBy.print();
	}
	
	public void tryToMapBadResults(){
		this.parents.tryToMapBadResults();
		this.symptoms.tryToMapBadResults();
		this.causedBy.tryToMapBadResults();
		this.healedBy.tryToMapBadResults();
	}
}
