package Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Collection;
import java.util.Comparator;
import java.util.TreeSet;

public class Disease extends MedicalItem {

	private final static TreeSet<String> DISEASES_SET = new TreeSet<String>(
			new Comparator<String>() {

				// FIXME bad practice
				public int compare(String arg0, String arg1) {
					int result = arg1.length() - arg0.length();
					if (result == 0 && !arg0.equals(arg1))
						result = 1;
					return result;
				}

			});

	public Disease(String name, Info mappingExcludedInfo, Info... infos) {
		super(name, mappingExcludedInfo, infos);
	}

	public static void parseDictionary(String s, Collection<String> c,
			boolean equals) {
		if (equals)
			for (String d : Disease.DISEASES_SET) {
				if (d.matches("^" + s + "$")) {
					c.add(d);
				}
			}
		else
			for (String d : Disease.DISEASES_SET) {
				if (s.matches("(^|.*[^a-z\\-0-9])" + d + "($|[^a-z\\-0-9].*)")) {
					s = s.replace(d, "");
					c.add(d);
				}
			}
	}

	protected void parseOwnDictionary(String s, Collection<String> c) {
		parseDictionary(s, c, false);
	}

	public static void addWord(String s) {
		DISEASES_SET.add(s);
	}

	public static void fillDictionnary(String File_name) {
		File file = new File(File_name);

		try {
			BufferedReader Br = new BufferedReader(new FileReader(file));

			String line;

			while ((line = Br.readLine()) != null) {
				line = line.trim().toLowerCase();
				if (line.length() > 0)
					DISEASES_SET.add(line);
			}

			Br.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
