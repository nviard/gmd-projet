package Utils;

import java.util.ArrayList;
import java.util.Collection;

public class MedicalCollection {

	private ArrayList<MedicalItem> results;
	private ArrayList<MedicalItem> badResults;

	public MedicalCollection() {
		this.results = new ArrayList<MedicalItem>();
		this.badResults = new ArrayList<MedicalItem>();
	}

	public void addMedicalItem(MedicalItem d) {
		if (d.containsNames()) {
			int index = results.indexOf(d);

			if (index >= 0) {
				results.get(index).addInfo(d.getInfo());
				results.get(index).addMappingExcludedInfo(
						d.getMappingExcludedInfo());
			} else {
				results.add(d);
			}
		} else {
			if(!tryToMapResult(d, this.badResults)){
				this.badResults.add(d);
			}
		}
	}

	public void print() {
		System.out.println(results.size());

		for (MedicalItem d : results) {
			System.out.println("Name : " + d);
			for (Info i : d.getInfo()) {
				System.out.println("Info : " + i);
			}
			for (Info i : d.getMappingExcludedInfo()) {
				
				System.out.println("Unmapped Info : " + i);
			
			}
		}
		System.err.println("Bad results : " + badResults.size());
		for (MedicalItem d : badResults) {
			System.err.println("Name : " + d);
			for (Info i : d.getInfo()) {
				System.err.println("Info : " + i);
			}
			for (Info i : d.getMappingExcludedInfo()) {
				System.err.println("Unmapped Info : " + i);
			}
		}
	}

	public void tryToMapBadResults() {
		for (int index = 0; index < badResults.size(); index++) {
			MedicalItem bad = badResults.get(index);
			if(tryToMapResult(bad, results)){
				badResults.remove(index);
				index --;
			}
		}
	}
	
	private boolean tryToMapResult(MedicalItem toMap, Collection<MedicalItem> dest){
		MedicalItem hypothesis = null;
		for (MedicalItem d : dest) {
			for (int index = 0; index < toMap.getInfo().size(); index++) {
				Info i = toMap.getInfo().get(index);
				if (d.getInfo().contains(i)) {
					if (hypothesis == null) {
						hypothesis = d;
					} else {
						toMap.addInfo(new Info(
								"Conflict between hypotheses, can be mapped more than once. ("
										+ i.getContent() + " in "
										+ d.toString() + ")",
								Info.Source.Conflict));
					}
					break;
				}
			}
		}
		if (hypothesis != null) {
			hypothesis.addInfo(toMap.getInfo(), true);
			hypothesis.addMappingExcludedInfo(toMap.getMappingExcludedInfo(), true);
			return true;
		}
		return false;
	}

	public ArrayList<MedicalItem> getResults() {
		return results;
	}

	public ArrayList<MedicalItem> getBadResults() {
		return badResults;
	}

	public void union(MedicalCollection c) {
		for (MedicalItem d : c.results) {
			this.addMedicalItem(d);
		}
		for (MedicalItem d : c.badResults) {
			this.addMedicalItem(d);
		}
	}

	public void inter(MedicalCollection c) {
		c.results.retainAll(this.results);
		this.results.retainAll(c.results);
		for (MedicalItem d : c.results) {
			this.addMedicalItem(d);
		}
		for (MedicalItem d : c.badResults) {
			this.addMedicalItem(d);
		}
	}
}
