package OrphaData;

import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

import org.codehaus.jackson.JsonNode;
import org.ektorp.CouchDbConnector;
import org.ektorp.CouchDbInstance;
import org.ektorp.ViewQuery;
import org.ektorp.ViewResult;
import org.ektorp.http.HttpClient;
import org.ektorp.http.StdHttpClient;
import org.ektorp.impl.StdCouchDbConnector;
import org.ektorp.impl.StdCouchDbInstance;

import Utils.Disease;
import Utils.Info;
import Utils.MedicalCollection;

public class ODRequestHandler {

	public CouchDbConnector db;
	private HashSet<String> diseasesKeys;
	private HashSet<String> synonymsKeys;
	private HashSet<String> clinicalSignsKeys;

	public ODRequestHandler() throws Exception {
		HttpClient authenticatedHttpClient = new StdHttpClient.Builder()
				.url("http://couchdb.telecomnancy.univ-lorraine.fr:80")
				.username("viard6u").password("CouchDB2A").build();
		CouchDbInstance dbInstance = new StdCouchDbInstance(
				authenticatedHttpClient);
		db = new StdCouchDbConnector("orphadatabase", dbInstance);

		ViewQuery query = new ViewQuery().designDocId("_design/diseases")
				.viewName("GetDiseasesBySynonym");
		ViewResult result = db.queryView(query);
		synonymsKeys = new HashSet<String>();
		for (ViewResult.Row row : result.getRows()) {
			synonymsKeys.add(row.getKey());
		}

		query = new ViewQuery().designDocId("_design/diseases").viewName(
				"GetDiseasesByName");
		result = db.queryView(query);
		diseasesKeys = new HashSet<String>();
		for (ViewResult.Row row : result.getRows()) {
			diseasesKeys.add(row.getKey());
		}

		query = new ViewQuery().designDocId("_design/clinicalsigns").viewName(
				"GetDiseaseByClinicalSign");
		result = db.queryView(query);
		clinicalSignsKeys = new HashSet<String>();
		for (ViewResult.Row row : result.getRows()) {
			String key = row.getKey();
			clinicalSignsKeys.add(row.getKey());
		}
	}

	public void close() {
	}

	private ViewResult getDiseasesBySynonym(String s) throws Exception {

		ViewQuery query = new ViewQuery().designDocId("_design/diseases")
				.viewName("GetDiseasesBySynonym");

		HashSet<String> keys = new HashSet<String>();
		for (String key : this.synonymsKeys) {
			if (key.toLowerCase().matches(
					"(^|.*[^a-z\\-0-9])" + s + "($|[^a-z\\-0-9].*)")) {
				keys.add(key);
			}
		}
		query.keys(keys);

		ViewResult result = db.queryView(query);

		return result;
	}

	private ViewResult getDiseasesByName(String s) throws Exception {
		ViewQuery query = new ViewQuery().designDocId("_design/diseases")
				.viewName("GetDiseasesByName");

		HashSet<String> keys = new HashSet<String>();
		for (String key : this.diseasesKeys) {
			if (key.toLowerCase().matches(
					"(^|.*[^a-z\\-0-9])" + s + "($|[^a-z\\-0-9].*)")) {
				keys.add(key);
			}
		}
		query.keys(keys);

		ViewResult result = db.queryView(query);

		return result;

	}

	private ViewResult getDiseaseClinicalSignsNoLang(int i) throws Exception {
		ViewQuery query = new ViewQuery().designDocId("_design/clinicalsigns")
				.viewName("GetDiseaseClinicalSignsNoLang").key(i);

		ViewResult result = db.queryView(query);

		return result;

	}

	private ViewResult getDiseases(int i) throws Exception {
		ViewQuery query = new ViewQuery().designDocId("_design/diseases")
				.viewName("GetDiseases").key(i);

		ViewResult result = db.queryView(query);

		return result;

	}

	private ViewResult getDiseaseByClinicalSign(String s) throws Exception {
		ViewQuery query = new ViewQuery().designDocId("_design/clinicalsigns")
				.viewName("GetDiseaseByClinicalSign");

		HashSet<String> keys = new HashSet<String>();
		for (String key : this.clinicalSignsKeys) {
			if (key.toLowerCase().matches(
					"(^|.*[^a-z\\-0-9])" + s + "($|[^a-z\\-0-9].*)")) {
				keys.add(key);
			}
		}
		query.keys(keys);

		ViewResult result = db.queryView(query);

		return result;
	}

	public Collection<String> getSynonyms(String s) throws Exception {
		HashSet<String> result = new HashSet<String>();
		ViewResult v = this.getDiseasesByName(s);
		for (ViewResult.Row r : v) {
			JsonNode synonyms = r.getValueAsNode().get("SynonymList")
					.get("Synonym");
			if (synonyms != null) {
				if (synonyms.isArray()) {
					JsonNode current;
					Iterator<JsonNode> iter = synonyms.getElements();
					while (iter.hasNext()) {
						current = iter.next();
						result.add(current.get("text").getTextValue()
								.toLowerCase());
					}
				} else {
					result.add(synonyms.get("text").getTextValue()
							.toLowerCase());
				}
			}
		}
		v = this.getDiseasesBySynonym(s);
		for (ViewResult.Row r : v) {
			JsonNode name = r.getValueAsNode().get("Name");
			if (name != null) {
				result.add(name.get("text").getTextValue().toLowerCase());
			}
		}
		return result;
	}

	public void fillParents(String s, MedicalCollection c) throws Exception {
		ViewResult v = this.getDiseaseByClinicalSign(s);
		for (ViewResult.Row r : v) {
			String id = r.getValueAsNode().get("disease").get("id")
					.getTextValue();
			ViewResult v2 = this.getDiseases(Integer.parseInt(id));
			for (ViewResult.Row r2 : v2) {
				String name = r2.getValueAsNode().get("Name").get("text")
						.getTextValue();
				JsonNode externalReferences = r2.getValueAsNode()
						.get("ExternalReferenceList").get("ExternalReference");
				if (externalReferences != null) {
					if (externalReferences.isArray()) {
						JsonNode current;
						Iterator<JsonNode> iter = externalReferences
								.getElements();
						while (iter.hasNext()) {
							current = iter.next();
							if (current.get("Source").getTextValue()
									.equals("UMLS")) {
								c.addMedicalItem(new Disease(name, new Info(s,
										Info.Source.OrphaData),
										new Info(current.get("Reference")
												.getTextValue(),
												Info.Source.OrphaData)));
							}
						}
					} else {
						if (externalReferences.get("Source").getTextValue()
								.equals("UMLS")) {
							c.addMedicalItem(new Disease(name, new Info(s,
									Info.Source.OrphaData), new Info(
									externalReferences.get("Reference")
											.getTextValue(),
									Info.Source.OrphaData)));
						}
					}
				}
			}

		}
	}

	public void fillSymptoms(String s, MedicalCollection c) throws Exception{
		ViewResult v = this.getDiseaseByClinicalSign(".*");
		for (ViewResult.Row r : v) {			
			String name = r.getValueAsNode().get("disease").get("Name").get("text")
					.getTextValue().toLowerCase();
			if(!name.matches("(^|.*[^a-z\\-0-9])" + s + "($|[^a-z\\-0-9].*)"))
				continue;
			String cs = r.getValueAsNode().get("clinicalSign").get("Name").get("text")
					.getTextValue().toLowerCase();
			c.addMedicalItem(new Disease(cs, new Info(name,
					Info.Source.OrphaData)));
		}
	}

}
