package DrugBank;
import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class DBIndexor extends DefaultHandler {
	
	public static final String INDEX_PATH = "drugbank_index";
	
	private Document doc;
	private String DrugXmlFileName;
	private String tmpValue;
	private Boolean baliseproduct=false;
	private Boolean balisepackagers=false;
	private Boolean balisedruginteraction=false;
	private Boolean balisepathway=false;
	private Boolean balisetarget=false;
	private Boolean balisepolypeptide=false;
	private Boolean balisemixture=false;
	private StringBuffer buffer= new StringBuffer();
	private int currentid;
	private IndexWriter writer;
	private int current=0;


	public DBIndexor(String drugXmlFileName) {
		this.DrugXmlFileName = drugXmlFileName;	

	}
	public void index() {		
		File INDEX_DIR = new File(INDEX_PATH);
		boolean create = true;

		try {
			Directory directory = FSDirectory.open(INDEX_DIR);
			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);
			IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40,
					analyzer);
			if (create) {
				config.setOpenMode(OpenMode.CREATE);
			} else {
				config.setOpenMode(OpenMode.CREATE_OR_APPEND);
			}

			writer = new IndexWriter(directory, config);
			parseDocument();
			writer.close();					
		}

		catch (IOException e) {
			System.out.println(" caught a " + e.getClass()
					+ "\n with message: " + e.getMessage());
		}
	}
	private void parseDocument() {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser parser = factory.newSAXParser();
			parser.parse(DrugXmlFileName,this );
		} catch (ParserConfigurationException e) {
			System.out.println("ParserConfig error");
		} catch (SAXException e) {
			System.out.println("SAXException : xml not well formed");
		} catch (IOException e) {

			System.out.println("IO error");
		}
	}

	public void startElement(String s, String s1, String elementName, Attributes attributes) throws SAXException {

		if (elementName.equalsIgnoreCase("drug")) {
			current=current+1;
			
			if (current==1){
				currentid=0;
				doc = new Document();
			}
		}
		
		if (elementName.equalsIgnoreCase("drugbank-id")) {
			currentid=currentid+1;
			if (currentid==1) {
				doc.add(new StoredField("primary", attributes.getValue("primary"))); 
			}
		}

		if(elementName.equalsIgnoreCase("product")) {
			baliseproduct=true;
		}
		if(elementName.equalsIgnoreCase("packagers")) {
			balisepackagers=true;
		}
		if(elementName.equalsIgnoreCase("drug-interaction")) {
			balisedruginteraction=true;
		}
		if(elementName.equalsIgnoreCase("pathway")) {
			balisepathway=true;
		}
		if(elementName.equalsIgnoreCase("target")) {
			balisetarget=true;
		}
		if(elementName.equalsIgnoreCase("polypeptide")) {
			balisepolypeptide=true;
		}
		if(elementName.equalsIgnoreCase("mixture")) {
			balisemixture=true;
		}
		if (elementName.equalsIgnoreCase("toxicity")) {
			buffer= new StringBuffer();
		}
		
	}


	public void endElement(String s, String s1, String element) throws SAXException {


		if (element.equals("drug") ) {
			if( current==1) {
				try {
					//System.out.println(doc.getFields());
					writer.addDocument(doc);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			current=current-1; 
		}
		if(element.equals("product")) {
			baliseproduct=false;
		}
		if(element.equals("packagers")) {
			balisepackagers=false;
		}
		if(element.equals("drug-interaction")) {
			balisedruginteraction=false;
		}
		if(element.equals("pathway")) {
			balisepathway=false;
		}
		if(element.equals("target")) {
			balisetarget=false;
		}
		if(element.equals("polypeptide")) {
			balisepolypeptide=false;
		}
		if(element.equals("mixture")) {
			balisemixture=false;
		}
		if (current==1 && balisedruginteraction==false && baliseproduct==false && balisepackagers==false && balisemixture==false && balisedruginteraction==false && balisetarget==false && balisepolypeptide==false && balisepathway==false )
		{	
			if (currentid==1){
				if (element.equals("drugbank-id")) {
					doc.add(new TextField("Drugbankid", tmpValue, Field.Store.YES));
				}
			}

			if (element.equals("toxicity")) {
				doc.add(new TextField("Toxicity", tmpValue, Field.Store.YES));
			}
			if (element.equals("name")) {
				doc.add(new TextField("Name", tmpValue, Field.Store.YES));

			}
			if (element.equals("indication")) {
				doc.add(new TextField("Indication", tmpValue, Field.Store.YES));
			}
		}
	}
	public void characters(char[] ac, int i, int j) throws SAXException {
		tmpValue = new String(ac, i, j);
		buffer.append(ac, i,  j);
	}
}
