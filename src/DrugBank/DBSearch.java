package DrugBank;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import Utils.Disease;
import Utils.Drug;
import Utils.Info;
import Utils.MedicalCollection;
import Utils.Results;

public class DBSearch {

	public static void Search(String line, String fields[], Results r) {

		String index = DBIndexor.INDEX_PATH;

		IndexReader reader;
		try {

			reader = DirectoryReader.open(FSDirectory.open(new File(index)));
			IndexSearcher searcher = new IndexSearcher(reader);
			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);
			BufferedReader in = null;

			int hitsPerPage = searcher.getIndexReader().maxDoc();
			in = new BufferedReader(new InputStreamReader(System.in, "UTF-8"));

			MultiFieldQueryParser parser = new MultiFieldQueryParser(
					Version.LUCENE_40, fields, analyzer);
			parser.setDefaultOperator(QueryParser.Operator.AND);


			Query query = parser.parse(line);

			searchDiseases(in, searcher, query, r, fields);

			reader.close();

		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
	}

	public static void Search(String line, String fields[],
			MedicalCollection results) {

		String index = DBIndexor.INDEX_PATH;

		IndexReader reader;
		try {

			reader = DirectoryReader.open(FSDirectory.open(new File(index)));
			IndexSearcher searcher = new IndexSearcher(reader);
			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);
			BufferedReader in = null;

			int hitsPerPage = searcher.getIndexReader().maxDoc();
			in = new BufferedReader(new InputStreamReader(System.in, "UTF-8"));

			MultiFieldQueryParser parser = new MultiFieldQueryParser(
					Version.LUCENE_40, fields, analyzer);
			parser.setDefaultOperator(QueryParser.Operator.AND);


			Query query = parser.parse(line);

			searchDrugs(in, searcher, query, results, fields);

			reader.close();

		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}

	}

	public static void searchDiseases(BufferedReader in,
			IndexSearcher searcher, Query query, Results results,
			String fields[]) throws IOException {

		TopDocs result = searcher.search(query, 1);
		ScoreDoc[] hits = result.scoreDocs;
		int numTotalHits = result.totalHits;

		// search only if we have hits
		if (numTotalHits != 0) {

			hits = searcher.search(query, numTotalHits).scoreDocs;

			Disease ind = null;
			Disease tox = null;

			// for all the hits
			for (int i = 0; i < numTotalHits; i++) {

				Document doc = searcher.doc(hits[i].doc);
				String longInfo = null;

				if (Arrays.asList(fields).contains("Name")) {
					longInfo = doc.get("Name").toLowerCase();
				}
				String toxicity = doc.get("Toxicity").toLowerCase().trim();
				String indication = doc.get("Indication").toLowerCase().trim();

				ind = new Disease(toxicity, new Info(toxicity,
						Info.Source.DrugBank));
				tox = new Disease(indication, new Info(indication,
						Info.Source.DrugBank));

				results.getParents().addMedicalItem(ind);
				;
				results.getSymptoms().addMedicalItem(tox);

			}

		}
	}

	public static void searchDrugs(BufferedReader in, IndexSearcher searcher,
			Query query, MedicalCollection results, String fields[])
			throws IOException {

		TopDocs result = searcher.search(query, 1);
		ScoreDoc[] hits = result.scoreDocs;
		int numTotalHits = result.totalHits;

		// search only if we have hits
		if (numTotalHits != 0) {

			hits = searcher.search(query, numTotalHits).scoreDocs;

			hits = searcher.search(query, numTotalHits).scoreDocs;

			Drug d = null;
			// for all the hits
			for (int i = 0; i < numTotalHits; i++) {

				Document doc = searcher.doc(hits[i].doc);

				String name = doc.get("Name").toLowerCase().trim();
				String longInfo = null;
				if (Arrays.asList(fields).contains("Toxicity")) {
					longInfo = doc.get("Toxicity");
				} else if (Arrays.asList(fields).contains("Indication")) {
					longInfo = doc.get("Indication");
				}
				if (longInfo == null)
					d = new Drug(name, null, new Info(name,
							Info.Source.DrugBank));
				else
					d = new Drug(name,
							new Info(longInfo, Info.Source.DrugBank), new Info(
									name, Info.Source.DrugBank));

				results.addMedicalItem(d);

			}
		}
	}
}
