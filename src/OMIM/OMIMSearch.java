package OMIM;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import Utils.Disease;
import Utils.Info;
import Utils.MedicalCollection;

public class OMIMSearch {

	private static String[] ID_FIELD = { "id" };

	public static HashSet<String> searchSynonyms(String param, String[] field)
			throws Exception {
		HashSet<String> result = new HashSet<String>();
		IndexReader reader;
		try {
			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);
			reader = DirectoryReader.open(FSDirectory.open(new File(
					OMIMIndexor.INDEX_PATH_CSV)));
			IndexSearcher searcher = new IndexSearcher(reader);

			MultiFieldQueryParser parser = new MultiFieldQueryParser(
					Version.LUCENE_40, field, analyzer);
			parser.setDefaultOperator(QueryParser.Operator.AND);
			Query query = parser.parse(param);

			doSynonymSearch(searcher, query, result, field);

			reader.close();

		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static void search(boolean isCSV, String param, String[] field,
			MedicalCollection results) throws Exception {

		String index = isCSV ? OMIMIndexor.INDEX_PATH_CSV
				: OMIMIndexor.INDEX_PATH;

		IndexReader reader;
		try {
			Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);
			reader = DirectoryReader.open(FSDirectory.open(new File(index)));
			IndexSearcher searcher = new IndexSearcher(reader);

			MultiFieldQueryParser parser = new MultiFieldQueryParser(
					Version.LUCENE_40, field, analyzer);
			parser.setDefaultOperator(QueryParser.Operator.AND);
			Query query = parser.parse(param);
			doSimpleSearch(searcher, query, isCSV, results, field);

			reader.close();

		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}

	}

	public static void doSimpleSearch(IndexSearcher searcher, Query query,
			boolean isCSV, MedicalCollection results, String[] fields)
			throws Exception {

		// Find the number of hits
		TopDocs result = searcher.search(query, 1);
		ScoreDoc[] hits = result.scoreDocs;
		int numTotalHits = result.totalHits;

		// search only if we have hits
		if (numTotalHits != 0) {
			hits = searcher.search(query, numTotalHits).scoreDocs;

			for (int i = 0; i < numTotalHits; i++) {

				Document doc = searcher.doc(hits[i].doc);
				if (isCSV) {
					String CUI = doc.get("CUI");
					String label = doc.get("label").trim().toLowerCase();
					if (CUI != null && label != null) {
						Disease d = new Disease(label, null, new Info(label,
								Info.Source.OMIM), new Info(CUI,
								Info.Source.OMIM));
						results.addMedicalItem(d);
					}
				} else {
					String title = doc.get("Title").toLowerCase().trim();
					if (Arrays.asList(fields).contains("Title")) {
						String symptoms = doc.get("Symptoms");
						if (symptoms != null) {
							symptoms = symptoms.trim();
							Disease d = new Disease(symptoms.toLowerCase(), new Info(
									symptoms, Info.Source.OMIM));
							results.addMedicalItem(d);
						}
					} else if (Arrays.asList(fields).contains("Symptoms")) {
						Disease d = new Disease(title, null, new Info(title,
								Info.Source.OMIM));
						results.addMedicalItem(d);
						
						String id = doc.get("id");
						search(true, id, ID_FIELD, results);
					}
				}
			}
		}
	}

	public static void doSynonymSearch(IndexSearcher searcher, Query query,
			HashSet<String> results, String[] fields) throws IOException {

		// Find the number of hits
		TopDocs result = searcher.search(query, 1);
		ScoreDoc[] hits = result.scoreDocs;
		int numTotalHits = result.totalHits;

		// search only if we have hits
		if (numTotalHits != 0) {
			hits = searcher.search(query, numTotalHits).scoreDocs;

			for (int i = 0; i < numTotalHits; i++) {
				
				Document doc = searcher.doc(hits[i].doc);
				
				String toAdd = "";
				if(Arrays.asList(fields).contains("label")){
					toAdd = doc.get("synonyms").trim();
				} else if(Arrays.asList(fields).contains("synonyms")){
					toAdd = doc.get("label").trim();
				}
				
				if(toAdd.length() != 0)
					results.add(toAdd.toLowerCase().trim());
			}
		}
	}
}
