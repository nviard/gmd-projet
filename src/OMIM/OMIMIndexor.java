package OMIM;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StoredField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.index.Term;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

/** Index all text files and CSV files under a directory. */

public class OMIMIndexor {
	public static final String INDEX_PATH = "omim_index";
	public static final String INDEX_PATH_CSV = "omim_index_csv";

	public static void indexOMIM(String fileToIndex,
			boolean isCSV) throws Exception {

		File INDEX_DIR = new File(isCSV ? INDEX_PATH_CSV : INDEX_PATH);
		boolean create = true;
		File file = new File(fileToIndex);
		Directory directory = FSDirectory.open(INDEX_DIR);
		Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);
		IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40,
				analyzer);
		if (create) {
			// Create a new index in the directory, removing any
			config.setOpenMode(OpenMode.CREATE);
		} else {
			config.setOpenMode(OpenMode.CREATE_OR_APPEND);
		}
		IndexWriter writer = new IndexWriter(directory, config);
		if (isCSV) {
			indexCSV(writer, file);
		} else {
			indexDoc(writer, file);
		}
		writer.close();

	}

	private static void indexCSV(IndexWriter writer, File file)
			throws IOException {

		if (file.canRead() && !file.isDirectory()) {
			// each line of the file is a new document
				InputStream ips = new FileInputStream(file);
				InputStreamReader ipsr = new InputStreamReader(ips);
				BufferedReader br = new BufferedReader(ipsr);
				String line;
				// initialization
				String id = "";
				String label = "";
				String synonyms = "";
				String CUI = "";
				
				// Parsing and indexing the file
				br.readLine();
				while ((line = br.readLine()) != null) {
					// Split fields
					String fields[]=line.split(";");
					if(fields.length <6){
						continue;
					}
					
					String tmp[] = fields[0].split("/");
					id = tmp[tmp.length-1];
					if(id.contains("T"))
						continue;
					if(id.contains("."))
						continue;
					
					label = fields[1];
					synonyms = fields[2];
					CUI = fields[5];

					// Create a document for each line
					Document doc = new Document();
					doc.add(new TextField("id", id, Field.Store.YES));// indexed
					// and
					// stored
					doc.add(new TextField("label", label, Field.Store.YES));// indexed
					// and
					// stored
					doc.add(new TextField("synonyms", synonyms, Field.Store.YES)); // indexed
					// and
					// stored
					doc.add(new TextField("CUI", CUI, Field.Store.YES)); // indexed
					// and
					// stored

					if (writer.getConfig().getOpenMode() == OpenMode.CREATE) {
						writer.addDocument(doc);
					} else {
						System.out.println("updating " + file);
						writer.updateDocument(new Term("path", file.getPath()),
								doc);
					}

					// clean values
					id="";
					label = "";
					synonyms = "";
					CUI = "";

				}
				
				br.close();

		}
	}

	private static void indexDoc(IndexWriter writer, File file)
			throws IOException {
		if (file.canRead() && !file.isDirectory()) {
			// each line of the file is a new document
				InputStream ips = new FileInputStream(file);
				InputStreamReader ipsr = new InputStreamReader(ips);
				BufferedReader br = new BufferedReader(ipsr);
				String line;
				// initialization
				String id = "";
				StringBuffer title = new StringBuffer("");
				StringBuffer symptoms = new StringBuffer("");
				boolean start = true;
				Document doc = new Document();
				while ((line = br.readLine()) != null) {
					if (line.equals("*RECORD*")) {
						if (start) {
							doc = new Document();
							br.readLine();
							id = br.readLine();
							//System.out.println("adding " + id);
							doc.add(new StoredField("id", id));
							start = false;
						} else {
							writer.addDocument(doc);
							doc = new Document();
							br.readLine();
							id = br.readLine();;
							//System.out.println("adding " + id);
							doc.add(new StoredField("id", id));
						}
					}

					if (line.equals("*FIELD* TI")) {
						while ((line = br.readLine()) != null) {
							if (line.startsWith("*FIELD*")) {
								break;
							} else {
								title.append(line).append(" ");

							}
						}
						doc.add(new TextField("Title", title.toString(), Field.Store.YES)); // Indexed
						// and
						// stored
					}

					if (line.equals("*FIELD* CS")) {
						while ((line = br.readLine()) != null) {
							if (line.startsWith("*FIELD*")) {
								break;
							} else {
								symptoms.append(line).append(" ");

							}
						}

						doc.add(new TextField("Symptoms", symptoms.toString(), Field.Store.YES));

					}
					id="";
					title = new StringBuffer("");
					symptoms = new StringBuffer("");
				}
				//adding the last record of OMIM
				writer.addDocument(doc);

				br.close();
		}
	}

}
