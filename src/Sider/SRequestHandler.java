package Sider;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import Utils.Disease;
import Utils.Drug;
import Utils.Info;
import Utils.MedicalCollection;

public class SRequestHandler {

	private final static String DB_SERVER = "jdbc:mysql://neptune.telecomnancy.univ-lorraine.fr:3306/";
	private final static String DB = "gmd";
	private final static String DRIVER = "com.mysql.jdbc.Driver";
	private final static String USER_NAME = "gmd-read";
	private final static String USER_PSWD = "esial";

	private Connection connection;
	private PreparedStatement psIndication;
	private PreparedStatement psDisease;
	private PreparedStatement psSymptoms;
	private PreparedStatement psParents;

	public SRequestHandler() throws Exception {
		Class.forName(DRIVER);
		connection = DriverManager.getConnection(DB_SERVER + DB, USER_NAME,
				USER_PSWD);

		String queryIndication = "SELECT drug_name1,drug_name2, i_name as name "
				+ "FROM indications_raw ,label_mapping "
				+ "WHERE indications_raw.label=label_mapping.label "
				+ "AND i_name=?";

		String queryDisease = "SELECT drug_name1,drug_name2,se_name as name "
				+ "FROM adverse_effects_raw ,label_mapping "
				+ "WHERE adverse_effects_raw.label=label_mapping.label "
				+ "AND se_name=?";

		String querySymptoms = "SELECT drug_name1 ,drug_name2 ,se_name as name "
				+ "FROM adverse_effects_raw ,label_mapping "
				+ "WHERE adverse_effects_raw.label=label_mapping.label "
				+ "AND (drug_name1=? OR drug_name2=?)";
		String queryParents = "SELECT drug_name1, drug_name2, i_name as name "
				+ "FROM indications_raw ,label_mapping "
				+ "WHERE indications_raw.label=label_mapping.label "
				+ "AND (drug_name1=? OR drug_name2=?)";

		psIndication = connection.prepareStatement(queryIndication);
		psDisease = connection.prepareStatement(queryDisease);
		psSymptoms = connection.prepareStatement(querySymptoms);
		psParents = connection.prepareStatement(queryParents);

		String queryMolecules = "SELECT * FROM pharmgkb_drug WHERE type = 'Drug/Small Molecule'";

		PreparedStatement csMolecules = connection
				.prepareStatement(queryMolecules);
		csMolecules.executeQuery();
		ResultSet res = csMolecules.getResultSet();
		while (res.next()) {
			Drug.addWord(res.getString("name").toLowerCase());
		}
		csMolecules.close();
	}

	public void close() throws Exception {
		psDisease.close();
		psIndication.close();
		connection.close();
	}

	public void execute(Boolean flag, String param, MedicalCollection results) {

		try {

			ResultSet res;

			if (flag) {
				psIndication.setString(1, param);
				psIndication.executeQuery();
				res = psIndication.getResultSet();
			} else {
				psDisease.setString(1, param);
				psDisease.executeQuery();
				res = psDisease.getResultSet();
			}

			while (res.next()) {

				String drug_name1 = res.getString("drug_name1").toLowerCase()
						.trim();
				String drug_name2 = res.getString("drug_name2").toLowerCase()
						.trim();

				if (drug_name1.length() > 0 && drug_name2.length() > 0) {
					results.addMedicalItem(new Drug(drug_name1 + ";"
							+ drug_name2, new Info(param, Info.Source.Sider),
							new Info(drug_name1, Info.Source.Sider), new Info(
									drug_name2, Info.Source.Sider)));
				} else if (drug_name1.length() > 0) {
					results.addMedicalItem(new Drug(drug_name1, new Info(param,
							Info.Source.Sider), new Info(drug_name1,
							Info.Source.Sider)));
				} else if (drug_name2.length() > 0) {
					results.addMedicalItem(new Drug(drug_name2, new Info(param,
							Info.Source.Sider), new Info(drug_name2,
							Info.Source.Sider)));
				}

			}
			res.close();

		} catch (SQLException ex) {
			System.err.println("SQLException information");
			while (ex != null) {
				System.err.println("Error msg: " + ex.getMessage());
				System.err.println("SQLSTATE: " + ex.getSQLState());
				System.err.println("Error code: " + ex.getErrorCode());
				ex.printStackTrace();
				ex = ex.getNextException();
			}

		}
	}

	public void executeDrug(Boolean flag, String param,
			MedicalCollection results) {

		try {

			ResultSet res;

			if (flag) {
				psSymptoms.setString(1, param);
				psSymptoms.setString(2, param);
				psSymptoms.executeQuery();
				res = psSymptoms.getResultSet();
			} else {
				psParents.setString(1, param);
				psParents.setString(2, param);
				psParents.executeQuery();
				res = psParents.getResultSet();
			}

			while (res.next()) {

				String name = res.getString("name").toLowerCase().trim();
				String drug_name1 = res.getString("drug_name1").toLowerCase()
						.trim();
				String drug_name2 = res.getString("drug_name2").toLowerCase()
						.trim();

				if (drug_name1.contains(param)) {
					results.addMedicalItem(new Disease(name, null, new Info(name,
							Info.Source.Sider)));
				}
				if (drug_name2.contains(param)) {
					results.addMedicalItem(new Disease(name, null, new Info(name,
							Info.Source.Sider)));
				}

			}
			res.close();

		} catch (SQLException ex) {
			System.err.println("SQLException information");
			while (ex != null) {
				System.err.println("Error msg: " + ex.getMessage());
				System.err.println("SQLSTATE: " + ex.getSQLState());
				System.err.println("Error code: " + ex.getErrorCode());
				ex.printStackTrace();
				ex = ex.getNextException();
			}

		}
	}

}
