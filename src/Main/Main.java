package Main;


import java.util.Scanner;

import Utils.Results;

public class Main {

	public static void main(String[] args) throws Exception {
		Mapper.prepare();
		
		System.out.println("Please enter a disease (q for quit) :");
		Scanner sc = new Scanner(System.in);
		String str = sc.nextLine();
		while(!str.equals("q")){
			Results r = Mapper.handleRequest(str);
			r.print();
			
			System.out.println("Please enter a disease (q for quit) :");
			str = sc.nextLine();
		}
		
		sc.close();
		Mapper.close();

	}

}
