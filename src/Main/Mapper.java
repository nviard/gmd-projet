package Main;

import java.util.HashSet;

import DrugBank.DBIndexor;
import DrugBank.DBSearch;
import OMIM.OMIMIndexor;
import OMIM.OMIMSearch;
import OrphaData.ODRequestHandler;
import Sider.SRequestHandler;
import Utils.Disease;
import Utils.Drug;
import Utils.Results;

public class Mapper {

	private static ODRequestHandler orphaData;
	private static SRequestHandler sider;
	private static String[] FIELDS_OMIM_NAME = { "Title", "label" };
	private static String[] FIELDS_OMIM_SYNONYMS = { "synonyms" };
	private static String[] FIELDS_OMIM_SYMPTOMS = { "Symptoms" };
	private final static String[] FIELDS_DRUGBANK_IND = { "Indication" };
	private final static String[] FIELDS_DRUGBANK_TOX = { "Toxicity" };
	private final static String[] FIELDS_DRUGBANK_NAME = { "Name" };

	public static void prepare() {
		try {
			System.out.print("Connecting to OrphaData... ");
			orphaData = new ODRequestHandler();
			System.out.println("Done !");
			System.out.print("Connecting to Sider... ");
			sider = new SRequestHandler();
			System.out.println("Done !");
			System.out.print("Indexing OMIM... ");

			OMIMIndexor.indexOMIM("omim.txt", false);
			OMIMIndexor.indexOMIM("omim_onto.csv", true);

			System.out.println("Done !");
			System.out.print("Indexing Drug Bank... ");
			new DBIndexor("drugbank.xml").index();
			System.out.println("Done !");

			System.out.print("Load dictionaries... ");
			Disease.fillDictionnary("Diseases_database.txt");
			Disease.fillDictionnary("Rare_diseases_database.txt");

			Disease.fillDictionnary("User_diseases_database.txt");
			Drug.fillDictionnary("User_molecules_database.txt");
			System.out.println("Done !");
			

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void close() {
		try {
			orphaData.close();
			sider.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static Results handleRequest(String s) throws Exception {
		boolean drugRequest = false;
		if (s.startsWith("%")) {
			drugRequest = true;
			s = s.substring(1);
		}

		Results result;
		Results tmp;

		String[] st = s.split("\\|\\|");
		result = handleRequest2(st[0], drugRequest);
		for (int i = 1; i < st.length; i++) {
			tmp = handleRequest2(st[i], drugRequest);
			result.union(tmp);
		}
		result.tryToMapBadResults();
		return result;
	}

	private static Results handleRequest2(String s, boolean drugRequest)
			throws Exception {
		Results result;
		Results tmp;

		String[] st = s.split("&&");
		result = simpleRequest(st[0], drugRequest);
		for (int i = 1; i < st.length; i++) {
			tmp = simpleRequest(st[i], drugRequest);
			result.inter(tmp);
		}
		return result;
	}

	private static Results fillSynonyms(Results r) throws Exception {
		HashSet<String> toAdd = new HashSet<String>();

		for (String s : r.getSynonyms()) {
			toAdd.addAll(OMIMSearch.searchSynonyms(s, FIELDS_OMIM_NAME));
			toAdd.addAll(OMIMSearch.searchSynonyms(s, FIELDS_OMIM_SYNONYMS));
			toAdd.addAll(orphaData.getSynonyms(s));
		}
		for (String tA : toAdd) {
			Disease.parseDictionary(tA, r.getSynonyms(), false);
		}
		return r;
	}

	private static Results fillDiseaseCollections(Results r) throws Exception {
		for (String s : r.getSynonyms()) {
			orphaData.fillParents(s, r.getParents());
			if(r.getSynonyms().size() < 3)
				orphaData.fillSymptoms(s, r.getSymptoms());
			OMIMSearch.search(false, s, FIELDS_OMIM_NAME, r.getSymptoms());
			OMIMSearch.search(false, s, FIELDS_OMIM_SYMPTOMS, r.getParents());

		}

		return r;
	}

	private static Results fillDiseaseCollectionsForDrugSearch(Results r) {
		for (String s : r.getSynonyms()) {
			sider.executeDrug(true, s, r.getSymptoms());

			sider.executeDrug(false, s, r.getParents());

			DBSearch.Search(s, FIELDS_DRUGBANK_NAME, r);

		}
		return r;
	}

	private static Results fillDrugCollections(Results r) {
		for (String s : r.getSynonyms()) {
			sider.execute(true, s, r.getHealedBy());
			sider.execute(false, s, r.getCausedBy());
			DBSearch.Search(s, FIELDS_DRUGBANK_IND, r.getHealedBy());
			DBSearch.Search(s, FIELDS_DRUGBANK_TOX, r.getCausedBy());
		}
		return r;
	}

	private static Results simpleRequest(String s, boolean drugRequest)
			throws Exception {
		Results r = new Results();
		s = s.trim().toLowerCase();
		if (drugRequest) {
			Drug.parseDictionary(s, r.getSynonyms(), true);
			r = fillDiseaseCollectionsForDrugSearch(r);
		} else {
			Disease.parseDictionary(s, r.getSynonyms(), true);
			
			r = fillSynonyms(r);

			r = fillDiseaseCollections(r);

			r = fillDrugCollections(r);

		}

		return r;
	}
}
